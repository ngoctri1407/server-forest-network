const Sequelize = require('sequelize');
import {sequelize} from '../db';


const Transactions = sequelize.define('Transactions', {
    version: {
        type: Sequelize.INTEGER
    },
    account: {
        type: Sequelize.STRING
    },
    sequence: {
        type: Sequelize.INTEGER
    },
    memo: {
        type: Sequelize.INTEGER
    },
    byte: {
        type: Sequelize.INTEGER
    },
    operation: {
        type: Sequelize.STRING
    },
    params: {
        type: Sequelize.TEXT
    },
    hash: {
        type: Sequelize.TEXT
    },
    signature: {
        type: Sequelize.STRING
    },
    height: {
        type: Sequelize.INTEGER
    }
});

export default Transactions;