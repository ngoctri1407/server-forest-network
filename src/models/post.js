const Sequelize = require('sequelize');
import {sequelize} from '../db';


const Post = sequelize.define('Post', {
    objectHash:{
        type: Sequelize.STRING
    },
    account: {
        type: Sequelize.STRING
    },
    content: {
        type: Sequelize.TEXT
    },
    params: {
        type: Sequelize.TEXT
    },
});

export default Post;