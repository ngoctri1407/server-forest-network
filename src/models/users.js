const Sequelize = require('sequelize');
import {sequelize} from '../db';

const Users = sequelize.define('Users', {
    address: {
        type: Sequelize.STRING
    },
    balance: {
        type: Sequelize.BIGINT
    },
    sequence: {
        type: Sequelize.INTEGER
    },
    following:{
        type:Sequelize.STRING
    },
    params:{
        type:Sequelize.TEXT
    }
});

export default Users;