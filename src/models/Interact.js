const Sequelize = require('sequelize');
import {sequelize} from '../db';


const Interact = sequelize.define('Interact', {
    objectHash:{
        type: Sequelize.STRING
    },
    account:{
        type: Sequelize.STRING
    },
    type: {
        type: Sequelize.INTEGER
    },
    content: {
        type: Sequelize.TEXT
    },
    params: {
        type: Sequelize.TEXT
    },
});

export default Interact;