import Transactions from './transactions';
import Info from './nodeInfo';
import Users from './users';
import Post from './post';
import Interact from './Interact';


// Users.hasMany(Post, {foreignKey: 'account', sourceKey: 'address'});
// Post.belongsTo(Users, {foreignKey: 'account', targetKey: 'account'});
//
Transactions.hasMany(Interact, {foreignKey: 'objectHash', sourceKey: 'hash'});
Interact.belongsTo(Transactions, {foreignKey: 'objectHash', sourceKey: 'objectHash'});

export default {Transactions,Info,Users,Post,Interact};