const Sequelize = require('sequelize');
import {sequelize} from '../db';

const Info = sequelize.define('Info', {
    last_block_height: {
        type: Sequelize.INTEGER
    },
    last_block_app_hash: {
        type: Sequelize.STRING
    }
});

export default Info;