const Sequelize = require('sequelize');
export var sequelize = new Sequelize('forest-network_dtb', 'forest-network_user', '123456', {
    host: 'forest-network_db',
    dialect: 'mysql',
    operatorsAliases: false,

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

export default callback => {
	// connect to a database if needed, then pass it to `callback`:
	callback(sequelize);
}
