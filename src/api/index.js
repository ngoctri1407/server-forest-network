import {version} from '../../package.json';
import {Router} from 'express';
import facets from './facets';

const {decode, verify, hash} = require('../lib/transaction');
const {decodeFollowing} = require('../lib/transaction/v1');
import model from '../models'
import * as Sequelize from "sequelize";
import Post from "../models/post";
const base32 = require('base32.js');
const superagent = require('superagent');

const listNode = [
    'komodo.forest.network',

    'zebra.forest.network',

    'dragonfly.forest.network',

    'gorilla.forest.network',
];

const randomServer = Math.floor(Math.random() * (4));

const server = listNode[randomServer];


function syncData() {
    const randomServer = Math.floor(Math.random() * (4));

    const server = listNode[randomServer];

    let dataBlockHeight = 1;
    //Get new information of node
    superagent.get(`https://${server}/abci_info`)
        .end((err, respone) => {
            if (err) {
                return console.log(err);
            }
            if (respone.body.result && respone.body.result.response && respone.body.result.response.last_block_height) {
                let lastBlockHeight = +respone.body.result.response.last_block_height;
                //Query information of node before
                model.Info.findOne({order: [['createdAt', 'DESC']]}).then((resInfo) => {
                    if (resInfo != undefined) {
                        dataBlockHeight = resInfo.dataValues.last_block_height;
                    }
                    else {
                        model.Info.create({
                            last_block_height: respone.body.result.response.last_block_height,
                            last_block_app_hash: respone.body.result.response.last_block_app_hash
                        });
                    }
                    //If have new block
                    if (+dataBlockHeight < +lastBlockHeight) {
                        resInfo.update({
                            last_block_height: respone.body.result.response.last_block_height,
                            last_block_app_hash: respone.body.result.response.last_block_app_hash
                        });
                        //Get all data and save to database
                        let index = +dataBlockHeight;
                        var myInterval = null;

                        myInterval = setInterval(() => {
                            if (index === +lastBlockHeight) {
                                clearInterval(myInterval);
                            }
                            const indexHeight = index;
                            const randomServer = Math.floor(Math.random() * (4));
                            const server = listNode[randomServer];
                            model.Transactions.findOne({where: {height: indexHeight}}).then(Transactions => {
                                if (Transactions == undefined) {
                                    superagent.get(`https://${server}/block?height=` + indexHeight)
                                        .end((err, responeBlock) => {
                                            if (err) {
                                                return console.log(err);
                                            }
                                            // console.log(indexHeight);
                                            if (responeBlock.body.result && responeBlock.body.result.block) {

                                                try {
                                                    if (responeBlock.body.result.block.data.txs != null) {
                                                        let data = decode(responeBlock.body.result.block.data.txs.toString());
                                                        const buffer = Buffer.from(responeBlock.body.result.block.data.txs.toString(),'base64');
                                                        model.Transactions.create({
                                                            hash:responeBlock.body.result.block.header.data_hash,
                                                            version: data.version,
                                                            account: data.account,
                                                            sequence: data.sequence,
                                                            memo: 0,
                                                            byte:buffer.length,
                                                            operation: data.operation,
                                                            params: JSON.stringify(data.params),
                                                            signature: '',
                                                            height: indexHeight
                                                        });
                                                        model.Users.findOne({
                                                            where: {
                                                                address: data.account
                                                            }
                                                        }).then((result) => {
                                                            if (result != undefined) {
                                                                result.update({
                                                                    sequence: data.sequence
                                                                });
                                                            }
                                                        });
                                                        if (data.operation == 'create_account') {
                                                            model.Users.findOne({
                                                                where: {
                                                                    address: data.params.address
                                                                }
                                                            }).then((result) => {
                                                                if (result == undefined) {
                                                                    model.Users.create({
                                                                        address: data.params.address,
                                                                        balance: 0,
                                                                        sequence: 1,
                                                                        following: JSON.stringify([]),
                                                                        params: '{}'
                                                                    });
                                                                }
                                                            });
                                                        }
                                                        else if (data.operation == 'update_account') {
                                                            model.Users.findOne({
                                                                where: {
                                                                    address: data.account
                                                                }
                                                            }).then((result) => {
                                                                if (result != undefined) {
                                                                    const params = JSON.parse(result.params);
                                                                    params[data.params.key] = data.params.value;
                                                                    result.update({
                                                                        params: JSON.stringify(params)
                                                                    })
                                                                }
                                                            });
                                                        }
                                                        else if (data.operation == 'payment') {
                                                            const params = data.params;
                                                            // decrease from user1
                                                            model.Users.findOne({
                                                                where: {
                                                                    address: data.account
                                                                }
                                                            }).then((result) => {
                                                                if (result != undefined) {
                                                                    result.update({
                                                                        balance: result.balance - +params.amount
                                                                    });
                                                                }
                                                                else {
                                                                    model.Users.create({
                                                                        address: data.account,
                                                                        balance: 0 - +params.amount,
                                                                        sequence: 1,
                                                                        following: JSON.stringify([]),
                                                                        params: '{}'
                                                                    });
                                                                }
                                                            });
                                                            // increase to user2
                                                            model.Users.findOne({
                                                                where: {
                                                                    address: params.address
                                                                }
                                                            }).then((result) => {
                                                                if (result != undefined) {
                                                                    result.update({
                                                                        balance: result.balance + +params.amount
                                                                    });
                                                                }
                                                                else {
                                                                    model.Users.create({
                                                                        address: params.address,
                                                                        balance: +params.amount,
                                                                        sequence: 1,
                                                                        following: JSON.stringify([]),
                                                                        params: '{}'
                                                                    });
                                                                }
                                                            });
                                                        }
                                                        else if(data.operation == 'post'){
                                                            const params = data.params;
                                                            if(params.content.text)
                                                                model.Post.create({
                                                                    objectHash:responeBlock.body.result.block.header.data_hash,
                                                                    account: data.account,
                                                                    content: params.content.text,
                                                                    params: '{}',
                                                                });
                                                        }
                                                        else if(data.operation == 'interact'){
                                                            const params = data.params;
                                                            model.Interact.findOne({
                                                                where:{
                                                                    objectHash:params.object,
                                                                    account: data.account,
                                                                    type:2
                                                                }
                                                            }).then(interact =>{
                                                                if(interact != undefined){
                                                                    interact.update({
                                                                        content: params.content.reaction,
                                                                    })
                                                                }
                                                                else {
                                                                    model.Interact.create({
                                                                        objectHash:params.object,
                                                                        account:data.account,
                                                                        type:params.content.type,
                                                                        content: params.content.type ==2 ? params.content.reaction:params.content.text,
                                                                        params: '{}',
                                                                    });
                                                                }
                                                            })
                                                        }
                                                    }
                                                    else {
                                                        model.Transactions.create({
                                                            hash:'',
                                                            version: 1,
                                                            account: '',
                                                            sequence: 0,
                                                            memo: 0,
                                                            operation: null,
                                                            params: '',
                                                            signature: '',
                                                            height: indexHeight
                                                        });
                                                    }
                                                }
                                                catch (e) {
                                                    console.log(e);
                                                }
                                                ;
                                            }
                                        });
                                }
                            });
                            index++;
                        }, 300);
                        // for (let indexHeight = +dataBlockHeight; indexHeight < +lastBlockHeight; indexHeight++)
                    }

                });


            }
        });
}

const interval = setInterval(() => syncData(), 30000);
model.Transactions.sync();
model.Users.sync();
model.Post.sync();
model.Info.sync();
model.Interact.sync();
model.Users.findOne().then(result => {
    if (result == undefined) {
        model.Users.create({
            address: 'GA6IW2JOWMP4WGI6LYAZ76ZPMFQSJAX4YLJLOQOWFC5VF5C6IGNV2IW7',
            balance: Math.pow(2, 53) - 1,
            sequence: 1,
            following: JSON.stringify([]),
            params: '{}'
        });
    }
});

function updateAccount() {
    model.Transactions.findAll({
        where: {
            operation: 'update_account'
        }
    }).then((transactions) => {
        if (transactions != undefined) {
            let lengthTrans = transactions.length;
            let indexCount = 0;
            let myInterval = null;

            myInterval = setInterval(() => {
                const index = indexCount;
                if (index === lengthTrans) {
                    clearInterval(myInterval);
                }
                try {
                    model.Users.findOne({
                        where: {
                            address: transactions[index].account
                        }
                    }).then((result) => {
                        if (result != undefined) {
                            const params = JSON.parse(result.params);
                            params[JSON.parse(transactions[index].params).key] = JSON.parse(transactions[index].params).value;
                            result.update({
                                params: JSON.stringify(params)
                            })
                        }
                    });
                }
                catch (e) {
                    console.log(e);
                }
                indexCount++;
            }, 200);
        }
    });
}

function GetDataUpdateAccount() {
    model.Transactions.findAll({
        where: {
            operation: 'interact'
        }
    }).then((transactions) => {
        if (transactions != undefined) {
            let lengthTrans = transactions.length;
            let indexCount = 0;
            let myInterval = null;

            myInterval = setInterval(() => {
                const index = indexCount;
                if (index === lengthTrans) {
                    clearInterval(myInterval);
                }
                try {
                    const randomServer = Math.floor(Math.random() * (4));

                    const server = listNode[randomServer];

                    superagent.get(`https://${server}/block?height=` + transactions[index].height)
                        .end((err, responeBlock) => {
                            if (err) {
                                return console.log(err);
                            }
                            // console.log(indexHeight);
                            if (responeBlock.body.result && responeBlock.body.result.block) {

                                try {
                                    if (responeBlock.body.result.block.data.txs != null) {
                                        let data = decode(responeBlock.body.result.block.data.txs.toString());
                                        transactions[index].update({
                                            params: JSON.stringify(data.params),
                                        });
                                    }
                                } catch (e) {
                                    console.log(e)
                                }
                            }})
                }
                catch (e) {
                    console.log(e);
                }
                indexCount++;
            }, 200);
        }
    });
}

function updateSequence() {
    model.Users.findAll().then((users) => {
        if (users != undefined) {
            let lengthUsers = users.length;
            let indexCount = 0;
            let myInterval = null;

            myInterval = setInterval(() => {
                const index = indexCount;
                if (index === lengthUsers-1) {
                    clearInterval(myInterval);
                }
                try {
                    model.Transactions.findOne({
                        where: {
                            account: users[index].address
                        },
                        order: [['createdAt', 'DESC']]
                    }).then((result) => {
                        if (result != undefined) {
                            users[index].update({
                                sequence: result.sequence
                            })
                        }
                    });
                }
                catch (e) {
                    console.log(e);
                }
                indexCount++;
            }, 200);
        }
    });
}


function createPost() {
    model.Transactions.findAll({
        where: {
            operation: 'post'
        }
    }).then((transactions) => {
        if (transactions != undefined) {
            let lengthTrans = transactions.length;
            let indexCount = 0;
            let myInterval = null;

            myInterval = setInterval(() => {
                const index = indexCount;
                if (index === lengthTrans-1) {
                    clearInterval(myInterval);
                }
                try {
                    if(JSON.parse(transactions[index].params).content.text)
                    model.Post.create({
                        objectHash:transactions[index].hash,
                        account: transactions[index].account,
                        content: JSON.parse(transactions[index].params).content.text,
                        params: '{}',
                    });
                }
                catch (e) {
                    console.log(e);
                }
                indexCount++;
            }, 200);
        }
    });
}
function createInteract() {
    model.Transactions.findAll({
        where: {
            operation: 'interact'
        }
    }).then((transactions) => {
        if (transactions != undefined) {
            let lengthTrans = transactions.length;
            let indexCount = 0;
            let myInterval = null;

            myInterval = setInterval(() => {
                const index = indexCount;
                if (index === lengthTrans-1) {
                    clearInterval(myInterval);
                }
                try {
                    model.Interact.findOne({
                        where:{
                            objectHash:JSON.parse(transactions[index].params).object,
                            account: transactions[index].account,
                            type:2
                        }
                    }).then(interact =>{
                        if(interact != undefined){
                            interact.update({
                                content: JSON.parse(transactions[index].params).content.reaction,
                            })
                        }
                        else {
                            model.Interact.create({
                                objectHash:JSON.parse(transactions[index].params).object,
                                account: transactions[index].account,
                                type:JSON.parse(transactions[index].params).content.type,
                                content: JSON.parse(transactions[index].params).content.type ==2 ? JSON.parse(transactions[index].params).content.reaction:JSON.parse(transactions[index].params).content.text,
                                params: '{}',
                            });
                        }
                    })
                }
                catch (e) {
                    console.log(e);
                }
                indexCount++;
            }, 200);
        }
    });
}

export default ({config, db}) => {
    let api = Router();

    // mount the facets resource
    api.use('/facets', facets({config, db}));

    // perhaps expose some API metadata at the root
    api.get('/', (req, res) => {
        res.json({version});
    });

    api.get('/getData', (req, res) => {
        syncData();
        model.Info.findOne({order: [['createdAt', 'DESC']]}).then((resInfo) => {
            if (resInfo != undefined) {
                res.send(resInfo.dataValues.last_block_height)
            }
            else {
                res.send('Get data done')
            }
        });
        res.send('Get data done')
    });
    api.get('/updateAccount', (req, res) => {
        updateAccount();
        res.json('update data done');
    });
    api.get('/GetDataUpdateAccount', (req, res) => {
        GetDataUpdateAccount();
        res.json('update data done');
    });
    api.get('/updateSequence', (req, res) => {
        updateSequence();
        res.json('update data done');
    });
    api.get('/createPost', (req, res) => {
        createPost();
        res.json('update data done');
    });
    api.get('/createInteract', (req, res) => {
        createInteract();
        res.json('update data done');
    });

    api.get('/stopGetData', (req, res) => {
        clearInterval(interval);
        res.json('Stop get data done');
    });

    api.get('/testdecodeFollowing', (req, res) => {
        // const postContent = decode('ATDWu4CH2zGdowQnEtrh97wGDp8eSyq6foVuU1x8Q6ipEpB7AAAAAAAAAIgAAwATABABAA1s4bqleSB4ZSBuw6BvAGb8oDhHPDwxbtHUjd2NIL72xE/9KZqvYy6PmROLwopFIw91OIkpZY7VLVBBSqJP79idkDn2w73D96kGrVnRMgI=')

        // const buffer = Buffer.from(postContent.params.content);
        const buffer1 = Buffer.from({"type":"Buffer","data":[0,2,48,73,223,131,193,134,201,52,152,145,46,74,46,155,192,128,4,40,166,240,87,131,27,39,93,224,13,19,137,173,208,27,120,32,75,48,127,183,60,244,200,183,25,12,148,210,241,94,207,236,32,27,168,107,199,196,30,60,32,141,51,67,67,219,162,184,33,234,37,206]},'base64');
        // console.log(buffer);
        const params = decodeFollowing({"type":"Buffer","data":[0,2,48,73,223,131,193,134,201,52,152,145,46,74,46,155,192,128,4,40,166,240,87,131,27,39,93,224,13,19,137,173,208,27,120,32,75,48,127,183,60,244,200,183,25,12,148,210,241,94,207,236,32,27,168,107,199,196,30,60,32,141,51,67,67,219,162,184,33,234,37,206]});
        console.log(base32.encode(params.addresses[0]));
        // res.send(Buffer.from('{"type":"Buffer","data":[1,0,6,49,50,51,49,50,52]}').toString());
        res.send(buffer1.toString());
    });

    api.post('/broadcast',(req,res) => {
        const tx = req.query.tx;
        superagent.get(`https://${server}/broadcast_tx_async?tx=` + tx)
            .end((err, responeBlock) => {
                if (err) {
                    res.send({error: 'Can not Broadcast'});
                }
                else {
                    res.send({status: 'success'});
                }
            })
    });

    api.get('/user/:address', (req, res) => {
        model.Users.findOne({
            where:{
                address:req.params.address
            }
        }).then(result =>{
            if(result !== undefined){
                let respone = {profile:result};
                respone.profile.params = JSON.parse(respone.profile.params);
                const params = respone.profile.params;
                var following=[];
                if(params.followings){
                    following = params.followings.addresses;
                }

                model.Users.findAll({
                    where:{
                        address:following
                    }
                }).then(users=> {
                    if (users !== undefined) {
                        var listFollowing = users;
                        respone.listFollowing = listFollowing;
                    }
                    model.Transactions.findAll({
                        where:{
                            account:result.address
                        },
                        order: [['sequence', 'ASC']]
                    }).then(trans=>{
                        if(trans !== undefined){
                            respone = Object.assign(respone,{transactions:trans});
                            res.send(respone);
                        }
                        else{
                            res.send(respone);
                        }
                    });
                });

            }
            else{
                res.send({
                    error:'Not found'
                })
            }
        });
    });

    api.get('/listUser/:address', (req, res) => {
        model.Users.findOne({
            where:{
                address:req.params.address
            }
        }).then(result =>{
            if(result !== undefined){
                let following = [];
                let respone = result;
                const params = JSON.parse(respone.params);
                if(params.followings){
                    following = params.followings.addresses;
                }

                model.Users.findAll().then(users=>{
                    if(users !== undefined){
                        var listUsers = users;
                        for(let i = 0;i<listUsers.length;i++){
                            listUsers[i] = listUsers[i].dataValues;
                            if(following.includes(listUsers[i].address)){
                                listUsers[i].isFollowing = true;
                            }
                            else{
                                listUsers[i].isFollowing = false;
                            }
                        }
                        res.send({followings:following,listUsers:listUsers});
                    }
                    // const Op = Sequelize.Op;
                    // model.Users.findAll({
                    //     where: {
                    //         address: {[Op.notIn]:following}
                    //     }
                    // }).then(unfollowing=>{
                    //     if(unfollowing !== undefined)
                    //     res.send({usersFollowing:userFollowing,usersUnfollowing:unfollowing});
                    // })

                });

            }
            else{
                res.send({
                    error:'Not found'
                })
            }
        });
    });

    api.get('/newFeed/:address', (req, res) => {
        const page = req.query.page || 1;
        const limit = req.query.limit || 30;

        model.Users.findOne({
            where:{
                address:req.params.address
            }
        }).then(result =>{
            if(result !== undefined){
                let following = [];
                let respone = result;
                const params = JSON.parse(respone.params);
                if(params.followings){
                    following = params.followings.addresses;
                }
                following.push(req.params.address);

                model.Transactions.findAll({
                    where:{
                        account:following,
                        operation:['post','update_account','payment','create_account']
                    },
                    include: [
                        {
                            model: model.Interact,
                        }],
                    offset: (page-1)*limit , limit: limit
                }).then(trans=>{
                    if(trans !== undefined){
                        var newFeed = trans;
                        res.send(newFeed);
                    }
                });

            }
            else{
                res.send({
                    error:'Not found'
                })
            }
        });
    });

    api.get('/timeline/:address', (req, res) => {
        const page = req.query.page || 1;
        const limit = req.query.limit || 30;

        model.Users.findOne({
            where:{
                address:req.params.address
            }
        }).then(result =>{
            if(result !== undefined){
                let respone = result;

                model.Transactions.findAll({
                    where:{
                        account:respone.address,
                        operation:['post','update_account','payment','create_account']
                    },
                    include: [
                        {
                            model: model.Interact,
                        }],
                    offset: (page-1)*limit , limit: limit
                }).then(trans=>{
                    if(trans !== undefined){
                        var timeline = trans;
                        res.send(timeline);
                    }
                });

            }
            else{
                res.send({
                    error:'Not found'
                })
            }
        });
    });


    api.get('/testDecode', (req, res) => {
        res.send(decode('ATBm+orKuKcF+VlvXHOX+CFLqbF889M2bRIqECQcgRWzt74jAAAAAAAAABEABQAsqXxbIONefhzreN5e0/fAB9dXJZezS84seWjbLwVQcJ8ACgEAB2NvbW1lbnRDwIP0yq8yLg2FtpCzjOnRTZuavpwlfVpFypP9fo9+kvujq4aSXZeAF0hE1QqSCG/E3PAL2qXmh/Puq8/70BcC'));
    });

    return api;
}
